<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Setoran;
use App\Pasar;
use App\Bank;
use App\UptdUser;

use Yajra\Datatables\Datatables;
use DB;
use App\Http\Requests\SetoranForm\SetoranFormRequest;
use App\Http\Requests\BankForm\BankFormRequest;
use File;

class SetoranController extends Controller
{

    public function index(){
      $user = Auth::user();
      $setoran = Setoran::all();
      return view('setoran.index',[]);
    }

    public function datatables(){
      $user = Auth::user();
      $pasar = Pasar::get()->pluck('id')->toArray();
      $uptdUser = UptdUser::where("user_id","$user->id")->first();
      if(Auth::user()->role_group != 'admin' && $uptdUser){
          $pasar = Pasar::where('uptd_id',$uptdUser->uptd_id)->pluck('id')->toArray();
      }

      DB::statement(DB::raw('set @rownum=0'));
      $setoran = Setoran::select('*', DB::raw('@rownum  := @rownum  + 1 AS rownum'))->with('bank','pasar.uptd','user')->whereIn('pasar_id',$pasar);
      return Datatables::of($setoran)
      ->addColumn('textSt', function($user){
          $st = '<center><span style="color:blue;border-bottom:1px dashed blue;padding-bottom:5px">Menunggu</span></center>';
          if($user->status == -1){
            $st = '<center><span style="color:red;border-bottom:1px dashed red;padding-bottom:5px">Ditolak</span></center>';
          }
          else if($user->status == 1){
            $st = '<center><span style="color:green;border-bottom:1px dashed green;padding-bottom:5px">Diterima</span></center>';
          }
          return "$st";
      })

      ->addColumn('actionButton', function($user){
          return $this->actionButton($user);
      })
      ->rawColumns(['actionButton','textSt'])
      ->make('true');
    }

    public function actionButton($user){
        $html = '<a href="'.url('setoran/view/'.$user->id).'" class="btn btn-primary btn-sm" data-button="ubah" data-id="'.$user->id.'">Lihat</a>';
        //$html .= '<button class="btn btn-danger btn-sm delete-button"  data-button="delete-button" data-id="'.$user->id.'">Hapus</button>';
        return $html;
    }



    public function create(){
      $user = Auth::user();
      $pasar = Pasar::all();
      $bank = Bank::all();
      $uptdUser = UptdUser::where("user_id","$user->id")->first();

      if(Auth::user()->role_group != 'admin' && $uptdUser){
          $pasar = Pasar::where('uptd_id',$uptdUser->uptd_id)->get();
      }

      return view('setoran.create',['pasar' => $pasar, 'bank' => $bank]);
    }


    public function store(SetoranFormRequest $request){
      $kdtrs =  $this->generateKodeSetoran();
      $setoran = new Setoran();
      $user = Auth::user();
      $filename = '';
      if ($request->file('image')) {
          $file = $request->file('image');
          $filename = md5($user->id . $user->name) . '.' . $file->getClientOriginalExtension();
          if(File::exists(public_path("images/bukti_transfer/").$filename)) {
              File::delete(public_path("images/bukti_transfer/").$filename);
          }

          $request->file('image')->move(public_path("images/bukti_transfer/"), $filename);
      }
      $setoran->image = $filename;
      $setoran->tanggal_setoran = date("$request->tanggal_setoran $request->hour:$request->minute:00");
      $setoran->bank_id = $request->bank;
      $setoran->keterangan = $request->keterangan;
      $setoran->pasar_id = $request->pasar;
      $setoran->jumlah = $request->jumlah;
      $setoran->no_setoran = $kdtrs;
      $setoran->status = 0;
      $setoran->created_by = $user->id;


      $setoran->save();
      return redirect('setoran')->with(['msg'=> 'Data telah tersimpan kedalam datatabase']);
    }

    public function generateKodeSetoran(){
        $now = date('Y-m-d');
        $setoran = Setoran::where('created_at','like',"$now%")->count();
        $head = '';
        $curQue = $setoran + 1;
        if($curQue < 10){
          $head = '0';
        }

        $kode = "TRS".date('Ymd').$head.$curQue;
        return $kode;
    }

    // public function update(Request $request){
    //   $setoran = Setoran::find($request->id);
    //   return view('setoran.update',['setoran' => $setoran]);
    // }

    // public function storeUpdate(SetoranFormRequest $request){
    //   $setoran = Setoran::find($request->id);
    //   $setoran->setoran_name = $request->setoran_name;
    //   $setoran->kode_rek = $request->kode_rek;
    //   $setoran->nama = $request->nama;
    //   $setoran->save();
    //   return redirect('setoran')->with(['msg'=> 'Data berhasil di perbarui !']);
    // }

    public function view(Request $request){
      // $setoran = Setoran::find($request->id)->with();
      $user = Auth::user();
      $pasar = Pasar::get()->pluck('id')->toArray();
      $uptdUser = UptdUser::where("user_id","$user->id")->first();
      if(Auth::user()->role_group != 'admin' && $uptdUser){
          $pasar = Pasar::where('uptd_id',$uptdUser->uptd_id)->pluck('id')->toArray();
      }
      $setoran = Setoran::with('bank','pasar.uptd','user')->whereIn('pasar_id',$pasar)->where('id',$request->id)->firstorfail();
      return view('setoran.view',['setoran' => $setoran]);
    }

    public function acc(Request $request){
      // $setoran = Setoran::find($request->id)->with();
      $user = Auth::user();
      $pasar = Pasar::get()->pluck('id')->toArray();
      $uptdUser = UptdUser::where("user_id","$user->id")->first();
      if(Auth::user()->role_group != 'admin' && $uptdUser){
          $pasar = Pasar::where('uptd_id',$uptdUser->uptd_id)->pluck('id')->toArray();
      }
      $setoran = Setoran::with('bank','pasar.uptd','user')->whereIn('pasar_id',$pasar)->where('id',$request->id)->firstorfail();
      $setoran->status = 1;
      $setoran->valid_date = date('Y-m-d H:i:s');
      $setoran->save();
      return redirect('setoran/view/'.$request->id)->with(['msg'=> 'Data telah tersimpan kedalam datatabase']);
      //return view('setoran.view',['setoran' => $setoran]);
    }

    public function rej(Request $request){
      // $setoran = Setoran::find($request->id)->with();
      $user = Auth::user();
      $pasar = Pasar::get()->pluck('id')->toArray();
      $uptdUser = UptdUser::where("user_id","$user->id")->first();
      if(Auth::user()->role_group != 'admin' && $uptdUser){
          $pasar = Pasar::where('uptd_id',$uptdUser->uptd_id)->pluck('id')->toArray();
      }
      $setoran = Setoran::with('bank','pasar.uptd','user')->whereIn('pasar_id',$pasar)->where('id',$request->id)->firstorfail();
      $setoran->status = -1;
      $setoran->valid_date = date('Y-m-d H:i:s');
      $setoran->save();
      return redirect('setoran/view/'.$request->id)->with(['msg'=> 'Data telah tersimpan kedalam datatabase']);
      //return view('setoran.view',['setoran' => $setoran]);
    }
    // public function delete(Request $request){
    //   $setoran = Setoran::find($request->id)->delete();
    //   return response()->json(['data' => $setoran]);
    // }
}
