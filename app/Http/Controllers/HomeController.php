<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $id = 1;
        //echo phpInfo();
        $user = Redis::get('user:id:'.$id);
        //Session::put('name','name');
        //echo Session::get('name');
        //var_dump($user);
        echo $user;
        exit;
        return view('user.index', ['user' => $user]);
        // $redis = Redis::connection();
        // $redis->set('name', 'Taylor');
        // $name = $redis->get('name');
        // echo $name;
        //return view('user.index');
    }
}
