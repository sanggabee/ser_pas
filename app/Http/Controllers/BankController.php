<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Bank;
use Yajra\Datatables\Datatables;
use DB;
use App\Http\Requests\BankForm\BankFormRequest;

class BankController extends Controller
{

    public function index(){
      $user = Auth::user();
      $bank = Bank::all();
      return view('bank.index',[]);
    }

    public function datatables(){
      DB::statement(DB::raw('set @rownum=0'));
      $bank = Bank::select('*', DB::raw('@rownum  := @rownum  + 1 AS rownum'));
      return Datatables::of($bank)
      ->addColumn('actionButton', function($user){
          return $this->actionButton($user);
      })
      ->rawColumns(['actionButton'])
      ->make('true');
    }

    public function actionButton($user){
        $html = '<a href="'.url('bank/update/'.$user->id).'" class="btn btn-primary btn-sm" data-button="ubah" data-id="'.$user->id.'">Ubah</a>';
        $html .= '<button class="btn btn-danger btn-sm delete-button"  data-button="delete-button" data-id="'.$user->id.'">Hapus</button>';
        return $html;
    }

    public function create(){
      return view('bank.create');
    }

    public function store(BankFormRequest $request){
      $bank = new Bank();
      $bank->bank_name = $request->bank_name;
      $bank->kode_rek = $request->kode_rek;
      $bank->nama = $request->nama;
      $bank->save();
      return redirect('bank')->with(['msg'=> 'Data telah tersimpan kedalam datatabase']);
    }

    public function update(Request $request){
      $bank = Bank::find($request->id);
      return view('bank.update',['bank' => $bank]);
    }

    public function storeUpdate(BankFormRequest $request){
      $bank = Bank::find($request->id);
      $bank->bank_name = $request->bank_name;
      $bank->kode_rek = $request->kode_rek;
      $bank->nama = $request->nama;
      $bank->save();
      return redirect('bank')->with(['msg'=> 'Data berhasil di perbarui !']);
    }

    public function delete(Request $request){
      $bank = Bank::find($request->id)->delete();
      return response()->json(['data' => $bank]);
    }
}
