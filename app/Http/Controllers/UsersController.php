<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User as Users;
use App\Uptd;
use Yajra\Datatables\Datatables;
use DB;
use App\Http\Requests\UserForm\UserFormRequest;
use App\Http\Requests\UserForm\UserUpdateFormRequest;

use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{
    public function index(){
      $user = Auth::user();
      $users = Users::all();
      return view('user.index',[]);
    }

    public function datatables(){
      DB::statement(DB::raw('set @rownum=0'));
      $users = Users::select('*', DB::raw('@rownum  := @rownum  + 1 AS rownum'));
      return Datatables::of($users)
      ->addColumn('actionButton', function($data){
          return $this->actionButton($data);
      })
      ->rawColumns(['actionButton'])
      ->removeColumn('password')
      ->make('true');
    }

    public function actionButton($data){
        $html = '<a href="'.url('user/update/'.$data->id).'" class="btn btn-primary btn-sm" data-button="ubah" data-id="'.$data->id.'">Ubah</a>';
        $html .= '<button class="btn btn-danger btn-sm delete-button"  data-button="delete-button" data-id="'.$data->id.'">Hapus</button>';
        return $html;
    }

    public function create(){
      $uptd = Uptd::all();
      return view('user.create',['uptd' => $uptd]);
    }

    public function store(UserFormRequest $request){
      $users = new Users();
      $users->name = $request->name;
      $users->email = $request->email;
      $users->password = Hash::make($request->password);
      $users->role_group = $request->role_group;
      $users->save();
      return redirect('user')->with(['msg'=> 'Data telah tersimpan kedalam datatabase']);
    }

    public function update(Request $request){
      $users = Users::find($request->id);
      return view('user.update',['user' => $users]);
    }

    public function storeUpdate(UserUpdateFormRequest $request){
      $users = Users::find($request->id);
      $users->name = $request->name;
      $users->email = $request->email;
      if($request->password != ''){
          $users->password = Hash::make($request->password);
      }
      $users->role_group = $request->role_group;
      $users->save();
      return redirect('user')->with(['msg'=> 'Data berhasil di perbarui !']);
    }

    public function delete(Request $request){
      $users = Users::find($request->id)->delete();
      return response()->json(['data' => $users]);
    }
}
