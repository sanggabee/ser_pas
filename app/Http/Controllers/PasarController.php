<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Pasar;
use App\Uptd;
use Yajra\Datatables\Datatables;
use DB;
use App\Http\Requests\PasarForm\PasarFormRequest;


class PasarController extends Controller
{
    //

    public function index(){
      $user = Auth::user();
      $pasar = Pasar::all();
      return view('pasar.index',[]);
    }

    public function datatables(){
      DB::statement(DB::raw('set @rownum=0'));
      $pasar = Pasar::select('*', DB::raw('@rownum  := @rownum  + 1 AS rownum'))->with('uptd');
      return Datatables::of($pasar)
      ->addColumn('actionButton', function($user){
          return $this->actionButton($user);
      })
      ->rawColumns(['actionButton'])
      ->make('true');
    }

    public function actionButton($user){
        $html = '<a href="'.url('pasar/update/'.$user->id).'" class="btn btn-primary btn-sm" data-button="ubah" data-id="'.$user->id.'">Ubah</a>';
        $html .= '<button class="btn btn-danger btn-sm delete-button"  data-button="delete-button" data-id="'.$user->id.'">Hapus</button>';
        return $html;
    }

    public function create(){
      $uptd = Uptd::all();
      return view('pasar.create',['uptd' => $uptd]);
    }

    public function store(PasarFormRequest $request){
      $pasar = new Pasar();
      $pasar->nama_pasar = $request->nama_pasar;
      $pasar->uptd_id = $request->uptd;
      $pasar->keterangan = $request->keterangan;
      $pasar->save();
      return redirect('pasar')->with(['msg'=> 'Data telah tersimpan kedalam datatabase']);
    }

    public function update(Request $request){
      $uptd = Uptd::all();
      $pasar = Pasar::find($request->id);
      return view('pasar.update',['uptd' => $uptd, 'pasar' => $pasar]);
    }

    public function storeUpdate(PasarFormRequest $request){
      $pasar = Pasar::find($request->id);
      $pasar->nama_pasar = $request->nama_pasar;
      $pasar->uptd_id = $request->uptd;
      $pasar->keterangan = $request->keterangan;
      $pasar->save();
      return redirect('pasar')->with(['msg'=> 'Data berhasil di perbarui !']);
    }

    public function delete(Request $request){
      $pasar = Pasar::find($request->id)->delete();
      return response()->json(['data' => $pasar]);
    }
}
