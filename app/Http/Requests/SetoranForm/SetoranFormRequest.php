<?php

namespace App\Http\Requests\SetoranForm;

use Illuminate\Foundation\Http\FormRequest;

class SetoranFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            //'tahun' => 'required',
            'tanggal_setoran' => 'required',
            'bank' => 'required|not_in:0',
            'pasar' => 'required|not_in:0',
            'keterangan' => 'required',
            'jumlah' => 'required',

            'image' => 'required|mimes:jpg,jpeg,JPEG,png,gif,bmp',
        ];
    }

    public function messages()
    {
        return [
          'required' => 'Form :attribute wajib di isi',
          'not_in' => 'Pilih :attribute yang tersedia',
          'mimes' => 'File :attribute yang diperbolehkan : jpg,jpeg,JPEG,png,gif,bmp ',
        ];
    }
}
