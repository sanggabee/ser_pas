<?php

namespace App\Http\Requests\PasarForm;

use Illuminate\Foundation\Http\FormRequest;

class PasarFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //'image' => 'mimes:jpg,jpeg,JPEG,png,gif,bmp', 'max:2024',
            //'tahun' => 'required',
            'nama_pasar' => 'required',
            'uptd' => 'required|not_in:0',
            'keterangan' => 'required',
        ];
    }

    public function messages()
    {
        return [
          'required' => 'Form :attribute wajib di isi',
          'not_in' => 'Pilih :attribute yang tersedia'
        ];
    }
}
