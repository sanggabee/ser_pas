<?php

namespace App\Http\Requests\UserForm;

use Illuminate\Foundation\Http\FormRequest;

class UserFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //'image' => 'mimes:jpg,jpeg,JPEG,png,gif,bmp', 'max:2024',
            //'tahun' => 'required',
            'name' => 'required',
            'email' => 'required|unique:users,email|email',
            'role_group' => 'required|not_in:0',
            'password' => 'required',
        ];
    }

    public function messages()
    {
        return [
          'required' => 'Form :attribute wajib di isi',
          'not_in' => 'Pilih :attribute yang tersedia',
          'email' => 'Format Email yang dimasukan salah',
          'unique' => 'Email telah di daftarkan'
        ];
    }
}
