<?php

namespace App\Http\Middleware;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Response;



//use Illuminate\Auth\Middleware\Authenticate as Middleware;

class SuperAdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    
    public function handle($request, Closure $next, $guard = null)
    {
        
        if($request->user() && $request->user()->role_group == 'admin'){
            return $next($request);
        }
        else if($request->user()){
            return new Response(view('unauthorized')->with('role', 'Admin'));
        }

        return redirect('/login');
        
    }
}
