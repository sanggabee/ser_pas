<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setoran extends Model
{
    //
    protected $table = 'setoran';

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'created_by');
    }

    public function bank()
    {
        return $this->hasOne('App\Bank', 'id', 'bank_id');
    }

    public function pasar()
    {
        return $this->hasOne('App\Pasar', 'id', 'pasar_id');
    }
}
