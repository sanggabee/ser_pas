<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UptdUser extends Model
{
    //
    protected $table = 'uptd_user';

    public function user(){
        return $this->hasMany('App\User', 'id', 'user_id');
    }

    public function uptd(){
        return $this->hasMany('App\Uptd', 'id', 'uptd_id');
    }
}
