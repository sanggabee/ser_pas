@extends('layouts.main_layout')

@section('title', 'Page Title')

<!-- @section('sidebar')
    @parent

    <p>This is appended to the master sidebar.</p>
@stop -->

@section('content')
<div class="bg-image overflow-hidden" style="background-image: url('assets/media/photos/photo3@2x.jpg');">
    <div class="bg-primary-dark-op">
        <div class="content content-narrow content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center mt-5 mb-2 text-center text-sm-left">
                <div class="flex-sm-fill">
                    <h1 class="font-w600 text-white mb-0 invisible" data-toggle="appear">Data Pasar</h1>
                    <h2 class="h4 font-w400 text-white-75 mb-0 invisible" data-toggle="appear" data-timeout="250">Dashboard > Pasar > Tambah Baru </h2>
                </div>

            </div>
        </div>
    </div>
</div>
                <!-- END Hero -->

                <!-- Page Content -->
<div class="content">
  <div class="block">
      <div class="block-header">
          <h3 class="block-title">Tambah <small>Data Pasar</small></h3>
      </div>
      <div class="block-content block-content-full">
            <div class="row">

                <div class="col-lg-12">
                    <!-- Form Horizontal - Default Style -->
                    <form class="mb-5" action="{{url('bank/storeupdate/').'/'.$bank->id}}" method="POST">
                        {{csrf_field()}}
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label" for="">Nama Bank</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control {{$errors->first('bank_name') ? 'is-invalid' : ''}}" name="bank_name" value="{{$bank->bank_name}}">

                                <div id="" class="invalid-feedback animated fadeIn">{{$errors ? $errors->first('bank_name') : '' }}</div>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label" for="example-hf-email">Kode Rekening</label>
                            <div class="col-sm-10">
                              <input type="text" class="form-control {{$errors->first('kode_rek') ? 'is-invalid' : ''}}" name="kode_rek" value="{{$bank->kode_rek}}">
                              <div id="" class="invalid-feedback animated fadeIn">{{$errors ? $errors->first('kode_rek') : '' }}</div>

                            </div>

                        </div>


                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label" for="">Nama Pemilik Rekening</label>
                            <div class="col-sm-10">
                                <textarea class="form-control {{$errors->first('nama') ? 'is-invalid' : ''}}" name="nama">{{$bank->nama}}</textarea>
                                <div id="" class="invalid-feedback animated fadeIn">{{$errors ? $errors->first('nama') : '' }}</div>

                            </div>
                        </div>


                        <hr>
                        <div class="form-group row">
                          <div class="col-sm-12">
                            <button class="btn btn-primary btn-sm" type="submit"> Simpan</button>
                          </div>
                        </div>



                    </form>
                </div>
            </div>
        </div>

  </div>
</div>
                <!-- END Page Content -->
@stop

@push('end-scripts')
<script>
</script>

@endpush
