@extends('layouts.main_layout')

@section('title', 'Page Title')

<!-- @section('sidebar')
    @parent

    <p>This is appended to the master sidebar.</p>
@stop -->

@section('content')

<div class="bg-image overflow-hidden" style="background-image: url('assets/media/photos/photo3@2x.jpg');">
    <div class="bg-primary-dark-op">
        <div class="content content-narrow content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center mt-5 mb-2 text-center text-sm-left">
                <div class="flex-sm-fill">
                    <h1 class="font-w600 text-white mb-0 invisible" data-toggle="appear">Data User</h1>
                    <h2 class="h4 font-w400 text-white-75 mb-0 invisible" data-toggle="appear" data-timeout="250">Dashboard > User </h2>
                </div>
                <div class="flex-sm-00-auto mt-3 mt-sm-0 ml-sm-3">
                    <span class="d-inline-block invisible" data-toggle="appear" data-timeout="350">
                        <a class="btn btn-primary px-4 py-2" data-toggle="click-ripple" href="{{url('user/create')}}">
                            <i class="fa fa-plus mr-1" onclick="jQuery('#toast-example-2').toast('show');"></i> Tambah Data User
                        </a>
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>
                <!-- END Hero -->

                <!-- Page Content -->

<div class="content">
  <div style="position: fixed; top: 2rem; right: 2rem; z-index: 9999999;">
      <!-- Toast Example 1 -->
        <div id="toast-example-1" class="toast fade hide" data-delay="4000" role="alert" aria-live="assertive" aria-atomic="true">
            <div class="toast-header">
                <i class="si si-bubble text-primary mr-2"></i>
                <strong class="mr-auto">Informasi </strong>
                <small class="text-muted">just now</small>
                <button type="button" class="ml-2 close" data-dismiss="toast" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="toast-body">
                Data yang dipilih berhasil terhapus dari database
            </div>
        </div>
        <!-- END Toast Example 1 -->

        <!-- Toast Example 2 -->
        <div id="toast-example-2" class="toast fade hide" data-delay="4000" role="alert" aria-live="assertive" aria-atomic="true">
            <div class="toast-header">
                <i class="si si-wrench text-danger mr-2"></i>
                <strong class="mr-auto">Informasi</strong>
                <small class="text-muted">just now</small>
                <button type="button" class="ml-2 close" data-dismiss="toast" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="toast-body">
              {{ session()->get( 'msg' ) }}
            </div>
        </div>
        <!-- END Toast Example 2 -->
    </div>

  <div class="block">
                        <div class="block-header">
                            <!-- <h3 class="block-title delete-button">Dynamic Table <small>Full</small></h3> -->
                        </div>
                        <div class="block-content block-content-full">
                            <!-- DataTables init on table by adding .js-dataTable-full class, functionality is initialized in js/pages/be_tables_datatables.min.js which was auto compiled from _es6/pages/be_tables_datatables.js -->
                            <div  class="dataTables_wrapper dt-bootstrap4 no-footer">
                              <table id="datatable" class="table table-bordered table-striped table-vcenter js-dataTable-simple">
                                <thead>
                                    <tr role="row">
                                      <th>
                                        No.
                                      </th>
                                      <th>
                                        Nama Pengguna
                                      </th>
                                      <th>
                                        Email
                                      </th>
                                      <th>
                                        Kelompok Pengguna
                                      </th>
                                      <th>
                                        Aksi
                                      </th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
    <!-- Stats -->

    <!-- END Dashboard Charts -->

    <!-- Customers and Latest Orders -->

    <!-- END Customers and Latest Orders -->
</div>
                <!-- END Page Content -->
@stop

@push('end-scripts')
<script>
// A $( document ).ready() block.

 $(function() {

      @if(session()->get( 'msg' ) != '')
        jQuery('#toast-example-2').toast('show')
      @endif
      $("#datatable").on("click", "[data-button=delete-button]", function(){
        if (confirm('You are about to delete this row, are you sure ?')) {
                 $.ajax({
                     url: "{{ url("user/delete")  }}/" + $(this).attr('data-id'),
                     type: "POST",
                     dataType: 'json',
                     headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                     success: function (res) {
                         jQuery('#toast-example-1').toast('show')
                         loadData();
                     },
                     error: function (res) {
                         window.alert(res.message);
                     }
                 })
             }
      });
      function loadData(){
        $('#datatable').DataTable({
            destroy: true,
            serverSide: true,
            processing: true,
            ajax: "{{url('user/datatables')}}",
            columns: [
                { data: 'rownum', name: 'rownum' },
                { data: 'name', name: 'name' },
                { data: 'email', name: 'email' },
                { data: 'role_group', name: 'role_group' },
                { data: 'actionButton', name: 'actionButton' },
            ],
            //dom: "<'row'<'col-sm-6'i><'col-sm-6'f>><'table-responsive'tr><'row'<'col-sm-6'l><'col-sm-6'p>>",
            language: {
                paginate: {
                    previous: "&laquo;",
                    next: "&raquo;"
                },
                search: "_INPUT_",
                searchPlaceholder: "Search"
            },
        });
      }



      loadData();


  });

</script>

@endpush
