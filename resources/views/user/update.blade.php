@extends('layouts.main_layout')

@section('title', 'Page Title')

<!-- @section('sidebar')
    @parent

    <p>This is appended to the master sidebar.</p>
@stop -->

@section('content')
<div class="bg-image overflow-hidden" style="background-image: url('assets/media/photos/photo3@2x.jpg');">
    <div class="bg-primary-dark-op">
        <div class="content content-narrow content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center mt-5 mb-2 text-center text-sm-left">
                <div class="flex-sm-fill">
                    <h1 class="font-w600 text-white mb-0 invisible" data-toggle="appear">Data User</h1>
                    <h2 class="h4 font-w400 text-white-75 mb-0 invisible" data-toggle="appear" data-timeout="250">Dashboard > Pengguna > Perbarui Pengguna </h2>
                </div>

            </div>
        </div>
    </div>
</div>
                <!-- END Hero -->

                <!-- Page Content -->
<div class="content">
  <div class="block">
      <div class="block-header">
          <h3 class="block-title">Tambah <small>Data User</small></h3>
      </div>
      <div class="block-content block-content-full">
            <div class="row">

                <div class="col-lg-12">
                    <!-- Form Horizontal - Default Style -->

                      <form class="mb-5" action="{{url('user/storeupdate/').'/'.$user->id}}" method="POST">

                        {{csrf_field()}}
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label" for="">Nama User</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control {{$errors->first('name') ? 'is-invalid' : ''}}" name="name" value="{{$user->name}}">

                                <div id="" class="invalid-feedback animated fadeIn">{{$errors ? $errors->first('name') : '' }}</div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label" for="">Email</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control {{$errors->first('email') ? 'is-invalid' : ''}}" name="email" value="{{$user->email}}" >

                                <div id="" class="invalid-feedback animated fadeIn">{{$errors ? $errors->first('email') : '' }}</div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label" for="">Kata Sandi</label>
                            <div class="col-sm-10">
                                <input type="password" class="form-control {{$errors->first('password') ? 'is-invalid' : ''}}" name="password">
                                <i style="font-size:12px">Kosongkan jika tidak ingin melakukan perubahan pada kata sangi</i>
                                <br>
                                <div id="" class="invalid-feedback animated fadeIn">{{$errors ? $errors->first('password') : '' }}</div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label" for="example-hf-email">Grup Pengguna</label>
                            <div class="col-sm-10">
                                  <select class="form-control {{$errors->first('role_group') ? 'is-invalid' : ''}}" name="role_group">
                                    <option value = '0'>Pilih Grup </option>
                                    <option value = 'admin' {{$user->role_group == 'admin' ? 'selected' : ''}}>Admin </option>
                                    <option value = 'uptd' {{$user->role_group == 'uptd' ? 'selected' : ''}}>UPTD </option>

                                </select>
                                <div id="" class="invalid-feedback animated fadeIn">{{$errors ? $errors->first('role_group') : '' }}</div>

                            </div>

                        </div>





                        <hr>
                        <div class="form-group row">
                          <div class="col-sm-12">
                            <button class="btn btn-primary btn-sm" type="submit"> Simpan</button>
                          </div>
                        </div>



                    </form>
                </div>
            </div>
        </div>

  </div>
</div>
                <!-- END Page Content -->
@stop

@push('end-scripts')
<script>
</script>

@endpush
