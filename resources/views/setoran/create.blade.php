@extends('layouts.main_layout')

@section('title', 'Page Title')

<!-- @section('sidebar')
    @parent

    <p>This is appended to the master sidebar.</p>
@stop -->

@section('content')
<div class="bg-image overflow-hidden" style="background-image: url('assets/media/photos/photo3@2x.jpg');">
    <div class="bg-primary-dark-op">
        <div class="content content-narrow content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center mt-5 mb-2 text-center text-sm-left">
                <div class="flex-sm-fill">
                    <h1 class="font-w600 text-white mb-0 invisible" data-toggle="appear">Data Setoran</h1>
                    <h2 class="h4 font-w400 text-white-75 mb-0 invisible" data-toggle="appear" data-timeout="250">Dashboard > Setoran > Tambah Baru </h2>
                </div>

            </div>
        </div>
    </div>
</div>
                <!-- END Hero -->

                <!-- Page Content -->
<div class="content">
  <div class="block">
      <div class="block-header">
          <h3 class="block-title">Tambah <small>Data Setoran</small></h3>
      </div>
      <div class="block-content block-content-full">
            <div class="row">
                <div class="col-lg-12">
                    <!-- Form Horizontal - Default Style -->
                    <form class="mb-5" action="{{url('setoran/store')}}" method="POST" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label" for="">Waktu Setoran</label>
                            <div class="col-sm-10">
                              <div class="row">
                                <div class="col-sm-4">
                                  <input type="text" name="tanggal_setoran" class="{{$errors->first('tanggal_setoran') ? 'is-invalid' : '' }} js-datepicker form-control" id="example-datepicker3" name="example-datepicker3" data-week-start="1" data-autoclose="true" data-today-highlight="true" data-date-format="yyyy-mm-dd" placeholder="Tahun-Bulan-Tanggal" data-date-end-date="0d">
                                  <div id="" class="invalid-feedback animated fadeIn">{{$errors ? $errors->first('tanggal_setoran') : '' }}</div>

                                </div>
                                <span style="padding:6px;">-&nbsp;&nbsp;</span>
                                <div class="col-md-1">
                                  <div class="row">
                                    <select name="hour" class="form-control" style="">
                                      @for($i = 0; $i < 24; $i++)
                                        <option>{{$i < 10 ? '0':''}}{{$i}}</option>
                                      @endfor
                                    </select>
                                  </div>
                                </div>
                                <span style="padding:10px;">:</span>
                                <div class="col-md-1">
                                  <div class="row">
                                    <select name="minute" class="form-control" style="">
                                      @for($i = 0; $i < 60; $i++)
                                        <option>{{$i < 10 ? '0':''}}{{$i}}</option>
                                      @endfor
                                    </select>
                                  </div>
                                </div>

                              </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label" for="example-hf-email">Pasar</label>
                            <div class="col-sm-10">
                                  <select class="form-control {{$errors->first('pasar') ? 'is-invalid' : ''}}" name="pasar">
                                    <option value = '0'>Pilih Pasar </option>
                                    @foreach($pasar as $key => $value)
                                      <option value="{{$value->id}}"> {{$value->nama_pasar}} </option>
                                    @endforeach
                                </select>
                                <div id="" class="invalid-feedback animated fadeIn">{{$errors ? $errors->first('pasar') : '' }}</div>

                            </div>

                        </div>

                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label" for="example-hf-email">Kode Rekening</label>
                            <div class="col-sm-10">
                                  <select class="form-control {{$errors->first('bank') ? 'is-invalid' : ''}}" name="bank">
                                    <option value = '0'>Pilih Rekening </option>
                                    @foreach($bank as $key => $value)
                                      <option value="{{$value->id}}"> ({{mb_strtoupper($value->bank_name)}}) {{$value->kode_rek}} - a/n : {{$value->nama}}</option>
                                    @endforeach
                                </select>
                                <div id="" class="invalid-feedback animated fadeIn">{{$errors ? $errors->first('bank') : '' }}</div>

                            </div>

                        </div>

                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label" for="">Keterangan</label>
                            <div class="col-sm-10">
                                <textarea class="form-control {{$errors->first('keterangan') ? 'is-invalid' : ''}}" name="keterangan"></textarea>
                                <div id="" class="invalid-feedback animated fadeIn">{{$errors ? $errors->first('keterangan') : '' }}</div>

                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label" for="">Jumlah</label>
                            <div class="col-sm-10">
                                <input type="number" class="form-control {{$errors->first('jumlah') ? 'is-invalid' : ''}}" name="jumlah">
                                <div id="" class="invalid-feedback animated fadeIn">{{$errors ? $errors->first('jumlah') : '' }}</div>

                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label" for="">Slip Setoran / Bukti Transaksi</label>
                            <div class="col-sm-10">
                                <input type="file" name="image" class="form-control {{$errors->first('image') ? 'is-invalid' : ''}}">
                                <div id="" class="invalid-feedback animated fadeIn">{{$errors ? $errors->first('image') : '' }}</div>

                            </div>
                        </div>



                        <hr>
                        <div class="form-group row">
                          <div class="col-sm-12">
                            <button class="btn btn-primary btn-sm" type="submit"> Simpan</button>
                          </div>
                        </div>



                    </form>
                </div>
            </div>
        </div>

  </div>
</div>
                <!-- END Page Content -->
@stop

@push('end-scripts')
<script>
</script>
<script src="{{ asset('template/oneui/src/assets/js/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
<script>jQuery(function(){ One.helpers(['datepicker']); });</script>

@endpush
