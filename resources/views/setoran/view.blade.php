@extends('layouts.main_layout')

@section('title', 'Page Title')

<!-- @section('sidebar')
    @parent

    <p>This is appended to the master sidebar.</p>
@stop -->

@section('content')
<!-- <div class="bg-image overflow-hidden" style="background-image: url('assets/media/photos/photo3@2x.jpg');">
    <div class="bg-primary-dark-op">
        <div class="content content-narrow content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center mt-5 mb-2 text-center text-sm-left">
                <div class="flex-sm-fill">
                    <h1 class="font-w600 text-white mb-0 invisible" data-toggle="appear">Data Setoran</h1>
                    <h2 class="h4 font-w400 text-white-75 mb-0 invisible" data-toggle="appear" data-timeout="250">Dashboard > Setoran > Lihat > {{$setoran->id}} </h2>
                </div>

            </div>
        </div>
    </div>
</div> -->
                <!-- END Hero -->

                <!-- Page Content -->
<div class="content">
  <div class="block">
      <div class="block-header">
          <h3 class="block-title">#{{$setoran->no_setoran}}</h3>
          <div class="block-options">
              <!-- Print Page functionality is initialized in Helpers.print() -->
              <button type="button" class="btn-block-option" onclick="One.helpers('print');">
                  <i class="si si-printer mr-1"></i> Cetak
              </button>
          </div>
      </div>
      <p class="font-size-sm text-muted text-center py-3 my-3 border-top" style="font-size:30px">
          PEMERINTAH KABUPATEN BANDUNG BARAT <BR>
            DINAS PERINDUSTRIAN PERDAGANGAN <BR>
              SURAT TANDA SETORAN (STS)
      </p>
      <div class="block-content">
          <div class="p-sm-4 p-xl-7">
              <div class="table-responsive push">
                  <table class="table table-bordered">
                      <thead>
                          <tr>
                              <th class="text-center" style="width: 60px;">No</th>
                              <th class="text-center">Kode Rekening</th>
                              <th class="text-center" style="width: 300px;">Uraian Rincian Obyek</th>
                              <th class="text-right" style="width: 120px;">Jumlah</th>

                          </tr>
                      </thead>
                      <tbody>

                          <tr>
                              <td class="text-center">1</td>
                              <td>
                                  {{$setoran->bank->kode_rek}}
                              </td>

                              <td class="text-LEFT">
                                RETRIBUSI LOS<BR>

                                  Setor Tanggal
                                  <?php
                                    $date=date_create($setoran->tanggal_setoran);
                                    echo date_format($date,"Y/m/d");
                                  ?>
                                  <br>
                                  {{$setoran->keterangan}}
                              </td>
                              <td class="text-right">Rp {{number_format($setoran->jumlah)}}</td>
                          </tr>

                          <tr>
                              <td colspan="3" class="font-w700 text-uppercase text-right bg-body-light">Jumlah</td>
                              <td class="font-w700 text-right bg-body-light">Rp {{number_format($setoran->jumlah)}}</td>
                          </tr>
                      </tbody>
                  </table>
              </div>
              <!-- END Table -->
              <div class="row mb-4">
                  <!-- Company Info -->
                  <div class="col-6 font-size-sm text-center">
                      <address>
                          Mengetahui<br>
                          An. Pengguna Anggaran / Kuasa Pengguna Anggaran<br>
                          Kasubah Penyusunan Program & Keungan
                          <br>
                          <br>
                          <br>
                          <br>
                          <br>
                          (Caca Permana, ST)
                          NIP. 1976072020110011002
                      </address>
                  </div>
                  <!-- END Company Info -->

                  <!-- Client Info -->
                  <div class="col-6 text-right font-size-sm text-center">
                      <address>

                        Bendahara Penerimaan /<br>
                        Bendahara Penerimaan Pembantu
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        (Nurhasana, S.Sos)
                        NIP. 19700121720007012008
                      </address>
                  </div>
                  <!-- END Client Info -->
              </div>
              <!-- Footer -->

              <!-- END Footer -->
              <div class="block-options">
                <hr>
                Bukti Slip Setoran
                  <!-- Print Page functionality is initialized in Helpers.print() -->
                  <img src="/images/bukti_transfer/{{$setoran->image}}" style="width:100%">
                  <hr>
                  <?php
                    $arrStatus = [
                      '-1' => 'Ditolak',
                      '0' => 'Menunggu',
                      '1' => 'Diterima',
                    ];
                  ?>
                  Status : {{$arrStatus[$setoran->status]}}
                  <br>
                  <br>
                  <a href="{{url('setoran/view/').'/'.$setoran->id.'/reject'}}" style="padding:5px 8px; margin:10px 0px;border:1px solid red;color:red">
                    Tolak
                  </a>

                  <a href="{{url('setoran/view/').'/'.$setoran->id.'/accept'}}" style="padding:5px 8px; margin:10px 10px;border:1px solid green;color:green">
                    Terima
                  </a>
              </div>
          </div>
      </div>
  </div>
</div>
                <!-- END Page Content -->
@stop

@push('end-scripts')
<script>
</script>

@endpush
