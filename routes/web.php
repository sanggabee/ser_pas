<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Auth::routes(['register' => false]);
//Auth::routes();
use Illuminate\Support\Facades\Redis;

Auth::routes();
// Route::get('/', function () {

//     return view('user.index');
// });

Route::get('/tes', function () {
    $user = Auth::user();
    echo $user;
    exit;
})->middleware('auth.admin');

Route::group(['middleware' => 'auth.admin'], function(){


    Route::get('/', function(){
        return view('user.index');
    });


    Route::group(['prefix' => '/pasar'],function(){
        Route::get('/','PasarController@index');
        Route::get('/create','PasarController@create');
        Route::post('/store','PasarController@store');
        Route::get('/update/{id}','PasarController@update');
        Route::post('/storeupdate/{id}','PasarController@storeUpdate');
        Route::post('/delete/{id}','PasarController@delete');
        Route::get('/datatables','PasarController@datatables');
    });

    Route::group(['prefix' => '/bank'],function(){
        Route::get('/','BankController@index');
        Route::get('/create','BankController@create');
        Route::post('/store','BankController@store');
        Route::get('/update/{id}','BankController@update');
        Route::post('/storeupdate/{id}','BankController@storeUpdate');
        Route::post('/delete/{id}','BankController@delete');
        Route::get('/datatables','BankController@datatables');
    });


    Route::group(['prefix' => '/setoran'],function(){
        Route::get('/','SetoranController@index');
        Route::get('/create','SetoranController@create');
        Route::post('/store','SetoranController@store');
        Route::get('/view/{id}','SetoranController@view');
        Route::get('/view/{id}/accept','SetoranController@acc');
        Route::get('/view/{id}/reject','SetoranController@rej');

        Route::post('/storeupdate/{id}','SetoranController@storeUpdate');
        Route::post('/delete/{id}','SetoranController@delete');
        Route::get('/datatables','SetoranController@datatables');
    });

    Route::group(['prefix' => '/user'],function(){
        Route::get('/','UsersController@index');
        Route::get('/create','UsersController@create');
        Route::post('/store','UsersController@store');
        Route::get('/update/{id}','UsersController@update');

        Route::post('/storeupdate/{id}','UsersController@storeUpdate');
        Route::post('/delete/{id}','UsersController@delete');
        Route::get('/datatables','UsersController@datatables');
    });


});
Route::get('/redis', function () {
    $visits = Redis::Incr('visits');

    return $visits;
});
Route::get('/home', 'HomeController@index')->name('home');
